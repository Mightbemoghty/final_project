# README #

### What is this repository for? ###

* Quick summary
* This project implements the Boston Mechanism algorithm to mimic the public school allocation process adopted by Boston Public Schools.

* The economic definitions of three characteristics provided below, along with the descriptions of three algorithms, are based on the materials used in UCLA Econ 145 by Prof. Ichiro Obara in Fall 2016.

* **Algorithm**
* Boston Mechanism (BM)

* **Definition**
* Step 1:
* + students submit the list of their favorite school
* Step 2:
* + for each school, generate an artificaial priority order of students within each priority class
* Step 3:
* + assign students to their first best school according to the priority order up to the capacity of each school
* Step 4:
* + students who were not assigned to any school apply the next available school on their list

### Contribution guidelines ###

* Writing tests
* + the capacity of all participants are assigned to 1 by default
* + make sure that the number of students and the capacity of school equals, otherwise there might be students without a school to attend
* + the txt files for test starts with the number of schools and students respective, then the ID (name for students, number for schools), and followed by list of preference


### Who do I talk to? ###

* Please feel free to contact me through email xdfcaiwensheng@gmail.com