#include "Allocation.h"
#include <cstdlib>
#include <iostream>

Participant::Participant() {
	capacity = 0;
}

Participant::Participant(int a) {
	capacity = a;
}

Participant::~Participant() {
	this->capacity = 0;
}

void Participant::update_capacity(int a) {
	this->capacity = a;
}

student::student() : Participant() {
	this->name = "";
	this->allocated = false;
}

/*
student::student(int a, vector<shared_ptr<school>> b, deque<shared_ptr<school>> c, string d)
	: Participant(a) {
	this->name = d;
	for (vector<shared_ptr<school>>::iterator it = b.begin(); it != b.end(); ++it) {
		this->match.push_back(*it);
	}
	for (deque<shared_ptr<school>>::iterator it = c.begin(); it != c.end(); ++it) {
		this->preference.push_back(*it);
	}
}
*/

student::~student() {
	this->Participant::~Participant();
	this->name = "";
}

void student::update_preference(shared_ptr<Participant> a) {
	this->preference.push_back(a);
}

void student::update_match(shared_ptr<Participant> a) {
	this->match.clear();
	this->match.push_back(a);
}

void student::update_id(string a) {
	this->name = a;
}

school::school() : Participant() {
	this->number = "";
	this->allocated = false;
}

/*
school::school(int a, vector<shared_ptr<student>> b, deque<shared_ptr<student>> c, string d)
	: Participant(a) {
	this->number = d;
	for (vector<shared_ptr<student>>::iterator it = b.begin(); it != b.end(); ++it) {
		this->match.push_back(*it);
	}
	for (deque<shared_ptr<student>>::iterator it = c.begin(); it != c.end(); ++it) {
		this->preference.push_back(*it);
	}
}
*/

school::~school() {
	this->Participant::~Participant();
	this->number = "";
}

void school::update_preference(shared_ptr<Participant> a) {
	this->preference.push_back(a);
}

void school::update_match(shared_ptr<Participant> a) {
	this->match.push_back(a);
}

void school::update_id(string a) {
	this->number = a;
}