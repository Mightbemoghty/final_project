#include <string>
#include <fstream>
#include <vector>
#include <queue>
#include <memory>

#ifndef ALLOCATION_H
#define ALLOCATION_H

using namespace std;

class Participant
{
public:
	Participant();
	Participant(int a);
	virtual ~Participant();
	virtual void update_preference(shared_ptr<Participant> a) {};
	virtual void update_match(shared_ptr<Participant> a) {};
	virtual void update_id(string a) {};
	void update_capacity(int a);
	virtual bool is_empty() { return true; };
	virtual bool is_allocated() { return false; };
	virtual void update_allocated(bool a) {};
	virtual string get_id() { return "none"; };
private:
	int capacity;
};

class student : public Participant 
{
public:
	student();
	//student(int a, vector<shared_ptr<school>> b, deque<shared_ptr<school>> c, string d);
	~student();
	void update_preference(shared_ptr<Participant> a);
	void update_match(shared_ptr<Participant> a);
	void update_id(string a);
	void update_allocated(bool a) { this->allocated = a; };
	bool is_empty() { return this->match.empty(); };
	bool is_allocated() { return this->allocated; };
	string get_id() { return name; };
	vector<shared_ptr<Participant>> get_match() { return match; }
	vector<shared_ptr<Participant>> get_preference() { return preference; };
private:
	string name;
	vector<shared_ptr<Participant>> match;
	vector<shared_ptr<Participant>> preference;
	bool allocated;
};

class school : public Participant
{
public:
	school();
	//school(int a, vector<shared_ptr<student>> b, deque<shared_ptr<student>> c, string d);
	~school();
	void update_preference(shared_ptr<Participant> a);
	void update_match(shared_ptr<Participant> a);
	void update_id(string a);
	bool is_empty() { return this->match.empty(); }
	void update_allocated(bool a) { this->allocated = a; };
	bool is_allocated() { return this->allocated; };
	string get_id() { return number; }
	vector<shared_ptr<Participant>> get_match() { return match; }
	vector<shared_ptr<Participant>> get_preference() { return preference; };
private:
	string number;
	vector<shared_ptr<Participant>> match;
	vector<shared_ptr<Participant>> preference;
	bool allocated;
};

#endif


