#include "Allocation.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

template<typename algo>
void allocation(vector<shared_ptr<student>> st, vector<shared_ptr<school>> sc, ofstream& out, algo alloc) {
	alloc(st, sc);
	out << "The result of allocation implementing the BM algorithm is:" << endl;
	int count = 0;
	for (auto & i : st) {
		out << i->get_id() << (i->get_match())->get_id() << endl;
		++count;
	}
}

/*TTC algorithm
void ttc(vector<shared_ptr<student>> st, vector<shared_ptr<school>> sc) {

}*/

//BM algorithm
void bm(vector<shared_ptr<student>> st, vector<shared_ptr<school>> sc) {
	int round = 0;
	for (auto & r : sc) {
		for (auto & i : st) {
			//student applies to the top choice
			shared_ptr<school> choice;
			choice = i->get_preference()[round];
			i->update_match(choice);
			//school forms the pool of candidates
			choice->update_match(i);
		}
		//school choose from candidates
		for (auto & i : sc) {
			vector<shared_ptr<Participant>> pref;
			pref = i->get_preference();
			vector<shared_ptr<Participant>> pool;
			pool = i->get_match();
			int count1 = 0;
			for (auto & j : st) {
				int count2 = 0;
				for (auto & k : pool) {
					if (pool[count2] == pref[count1]) {
						i->update_allocated(true);
						pool[count2]->update_allocated(true);
						break;
					}
					++count2;
				}
			++count1;
			}
		}
		++round;
	}
}

/*DA + SI algorithm
void dasi(vector<shared_ptr<student>> st, vector<shared_ptr<school>> sc) {

}*/

int main() {
	//vector that holds all students and schools respectively
	vector<shared_ptr<student>> studentslist;
	vector<shared_ptr<school>> schoolslist;
	cout << "This is the Boston Public School Allocation Simulation.\n" 
		<< "Please adhere to the guidelines of writing a test case if you want to use your own test other the default..."
		<< endl;
	ifstream studentin;
	ifstream schoolin;
	studentin.open("student.txt");
	schoolin.open("school.txt");
	if (studentin.fail()) {
		cout << "Error opening " << "student.txt\n";
		return 1;
	}
	
	if (schoolin.fail()) {
		cout << "Error opening " << "school.txt\n";
		return 1;
	}
	
	//read in data from the text file
	int total_school;
	int total_student;
	studentin >> total_student;
	schoolin >> total_school;

	//initialize list of students and schools
	for (int i = 0; i < total_student; ++i) {
		shared_ptr<student> st(new student);
		studentslist.push_back(st);
	}
	for (int i = 0; i < total_school; ++i) {
		shared_ptr<school> sc(new school);
		schoolslist.push_back(sc);
	}
	
	//update data of students; match reamins default since students are not assigned to any school initially
	for (int i = 0; i < total_student; ++i) {
		string name;
		studentin >> name;
		studentslist[i]->update_id(name);
		int capa;
		studentin >> capa;
		studentslist[i]->update_capacity(capa);
		//read in all preference
		for (int j = 0; j < total_school; ++j) {
			string number;
			studentin >> number;
			for (int k = 0; k < total_school; ++k) {
				if (schoolslist[k]->get_id() == number) {
					studentslist[i]->update_preference(schoolslist[k]);
				}
			}
		}
	}

	studentin.close();
	
	//update data of schools; match reamins default since schools are not assigned to any student initially
	for (int i = 0; i < total_school; ++i) {
		string number;
		schoolin >> number;
		schoolslist[i]->update_id(number);
		int capa;
		schoolin >> capa;
		schoolslist[i]->update_capacity(capa);
		//read in all preference
		for (int j = 0; j < total_student; ++j) {
			string name;
			schoolin >> name;
			for (int k = 0; k < total_student; ++k) {
				if (studentslist[k]->get_id() == name) {
					schoolslist[i]->update_preference(studentslist[k]);
				}
			}
		}
	}
	
	schoolin.close();

	ofstream resultout;
	resultout.open("result.txt");
	if (resultout.fail()) {
		cout << "Error opening " << "result.txt\n";
		return 1;
	}
	//apply TTC and record the result
	//allocation(studentslist, schoolslist, resultout, ttc);
	//apply BM and record the result
	allocation(studentslist, schoolslist, resultout, bm);
	//apply DA+SI and record the result
	//allocation(studentslist, schoolslist, resultout, dasi);
	resultout.close();
	string choice;
	cin >> choice;
	return 0;
}